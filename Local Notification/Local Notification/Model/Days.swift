//
//  Days.swift
//  Local Notification
//
//  Created by Muchamad Fauzi on 25/05/23.
//

import Foundation

enum Day:Int, Codable, CaseIterable{
    case Sun = 0,Mon,Tue,Wed,Thu,Fri,Sat
    
    var dayString:String{
        switch self {
            case .Sun: return "Setiap Minggu"
            case .Mon: return "Setiap Senin"
            case .Tue: return "Setiap Selasa"
            case .Wed: return "Setiap Rabu"
            case .Thu: return "Setiap Kamis"
            case .Fri: return "Setiap Jumat"
            case .Sat: return "Setiap Sabtu"
        }
    }
    
    var dayText:String{
        switch self {
            case .Sun: return "Min"
            case .Mon: return "Sen"
            case .Tue: return "Sel"
            case .Wed: return "Rab"
            case .Thu: return "Kam"
            case .Fri: return "Jum"
            case .Sat: return "Sab"
        }
    }
    
    var componentWeekday: Int {
        switch self {
            case .Sun: return 1
            case .Mon: return 2
            case .Tue: return 3
            case .Wed: return 4
            case .Thu: return 5
            case .Fri: return 6
            case .Sat: return 7
        }
    }
    
}
