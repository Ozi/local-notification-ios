//
//  NotifInfo.swift
//  Local Notification
//
//  Created by Muchamad Fauzi on 25/05/23.
//

import Foundation
import UserNotifications

struct NotifInfo: Codable {
    
    var id = UUID()
    var date:Date = Date()
    var note:String = "Notification"
    var noteLabel:String{
        if repeatDay == "Belum Diset"{
            return note
        }
        return note + ", " + repeatDay
    }
    
    var isOn: Bool = true {
        didSet{
            if isOn{
                UserNotification.shared.addNotificationRequest(notif: self)
            }
        }
    }
    
    var selectDays:Set<Day> = []
    var isEdit = true
    
    var repeatDay:String{
        switch selectDays{
        case [.Sat, .Sun]:
            return "Weekend"
        case [.Sun, .Mon, .Tue, .Wed, .Thu, .Fri, .Sat]:
            return "Setiap Hari"
        case [.Mon, .Tue, .Wed, .Thu, .Fri]:
            return "Hari Biasa"
        case []:
            return "Belum Diset"
        default:
            let day = selectDays.sorted(by: {$0.rawValue < $1.rawValue}).map{$0.dayText}.joined(separator: " ,")
            return day
        }
    }
}


