//
//  NotifStore.swift
//  Local Notification
//
//  Created by Muchamad Fauzi on 26/05/23.
//

import Foundation
import UserNotifications

struct NotifStore {
    
    private let userDefault = UserDefaults.standard
    var isEdit = false
    private(set) var notifi:[NotifInfo] = []{
        didSet{
            notifi.sort{ $0.date < $1.date }
            saveData()
        }
    }
    
    init(){
        loadData()
    }
    
    mutating func isSwitch(_ index: Int,_ tableViewCellisOn:Bool){
        notifi[index].isOn = tableViewCellisOn
    }
    
    mutating func edit(_ notifData:NotifInfo,_ index: Int){
        notifi[index] = notifData
    }
    
    mutating func remove(_ index:Int){
        let removedData = notifi.remove(at: index)
        saveData()
        cancelNotification(for: removedData)
    }
    
    mutating func append(_ notifData:NotifInfo){
        notifi.append(notifData)
    }
    
    private func cancelNotification(for notifData: NotifInfo) {
        let center = UNUserNotificationCenter.current()
        
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: notifData.date)
        let minute = calendar.component(.minute, from: notifData.date)
        
        if notifData.selectDays.isEmpty {
            var dateComponents = DateComponents()
            dateComponents.hour = hour
            dateComponents.minute = minute
            let identifier = "\(notifData.id.uuidString)-0"
            center.removePendingNotificationRequests(withIdentifiers: [identifier])
        } else {
            let weekdays = notifData.selectDays.map { $0.componentWeekday }
            weekdays.forEach { weekDay in
                var dateComponents = DateComponents()
                dateComponents.hour = hour
                dateComponents.minute = minute
                dateComponents.weekday = weekDay
                let identifier = "\(notifData.id.uuidString)-\(weekDay)"
                center.removePendingNotificationRequests(withIdentifiers: [identifier])
            }
        }
        
        // Cancel delivered notifications
        print("Reminder Deleted")
        let identifier = notifData.id.uuidString
        center.getDeliveredNotifications { deliveredNotifications in
            let matchingNotifications = deliveredNotifications.filter { notification in
                let components = notification.request.identifier.components(separatedBy: "-")
                return components.first == identifier
            }
            let notificationIdentifiers = matchingNotifications.map { $0.request.identifier }
            center.removeDeliveredNotifications(withIdentifiers: notificationIdentifiers)
        }
    }

    
    private func saveData(){
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(notifi) {
            let defaults = userDefault
            defaults.set(encoded, forKey: "data")
        }
    }
    
    private mutating func loadData(){
        if let saveData = userDefault.object(forKey: "data") as? Data {
            let decoder = JSONDecoder()
            if let loadedData = try? decoder.decode(Array<NotifInfo>.self, from: saveData) {
                notifi = loadedData
            }
        }
    }
    
}
