//
//  NotifTableViewCell.swift
//  Local Notification
//
//  Created by Muchamad Fauzi on 02/06/23.
//

import UIKit

class NotifTableViewCell: UITableViewCell {
    
    var indicatorView: UIView = {
       let dot = UIView()
        dot.frame = CGRect(x: 0, y: 0, width: 20, height: 20)
        dot.layer.cornerRadius = dot.frame.size.width/2
        dot.clipsToBounds = true
        dot.backgroundColor = .green
        
        return dot
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .secondarySystemFill

        self.accessoryView = indicatorView
        self.editingAccessoryType = .disclosureIndicator
        setupUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI(){
        textLabel?.font = UIFont.systemFont(ofSize: 50)
        detailTextLabel?.font = UIFont.systemFont(ofSize: 14)
    }
}
