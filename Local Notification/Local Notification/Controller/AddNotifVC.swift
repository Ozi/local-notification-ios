//
//  AddNotifVC.swift
//  Local Notification
//
//  Created by Muchamad Fauzi on 25/05/23.
//

import UIKit
import SnapKit

protocol SaveNotifInfoDelegate:AnyObject{
    func saveNotifInfo(notifData: NotifInfo, index: Int)
}

class AddNotifVC: UIViewController {
    
    var contentItems: [ContentItem] {
        [
            .days(notif.repeatDay),
            .label(notif.note),
            .sounds("Call_Ringtone")
        ]
    }
    
    var notif = NotifInfo(){
        didSet{
            datePicker.date = notif.date
            tableView.reloadData()
        }
    }

    let datePicker:UIDatePicker = {
        let myPicker = UIDatePicker()
        myPicker.datePickerMode = .time
        myPicker.locale = Locale(identifier: "NL")
        myPicker.preferredDatePickerStyle = .wheels
        return myPicker
    }()
    
    let tableView:UITableView = {
        let myTable = UITableView()
        myTable.layer.cornerRadius = 10
        myTable.isScrollEnabled = false
        myTable.register(AddNotifTableViewCell.self, forCellReuseIdentifier: AddNotifTableViewCell.identifier)
        
        return myTable
    }()
    
    var tempIndexRow:Int = 0
    
    weak var saveNotifDataDelegate: SaveNotifInfoDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .brown
        overrideUserInterfaceStyle = .dark
        setupUI()
        setupNavigation()
        goToSettings()
    }
    
    func goToSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            
            DispatchQueue.main.async {
                if (settings.authorizationStatus != .authorized) {
                    let ac = UIAlertController(title: "Enable Notifications?", message: "To use this feature you must enable notifications in settings", preferredStyle: .alert)
                    let goToSettings = UIAlertAction(title: "Settings", style: .default) {_ in
                        guard let settingsURL = URL(string: UIApplication.openSettingsURLString)else
                        {
                            return
                        }
                        
                        if UIApplication.shared.canOpenURL(settingsURL) {
                            UIApplication.shared.open(settingsURL)
                        }
                    }
                    ac.addAction(goToSettings)
                    ac.addAction(UIAlertAction(title: "Cancel", style: .default))
                    self.present(ac, animated: true)
                }
            }
        }
    }

    func setupUI(){
        
        tableView.dataSource = self
        tableView.delegate = self
        self.view.addSubview(datePicker)
        self.view.addSubview(tableView)
        
        datePicker.snp.makeConstraints { make in
            make.top.equalTo(view).offset(48)
            make.width.equalTo(view)
        }

        tableView.snp.makeConstraints { make in
            make.leading.equalTo(view.safeAreaLayoutGuide).offset(18)
            make.centerX.equalTo(view.safeAreaLayoutGuide)
            make.top.equalTo(datePicker.snp.bottom).offset(42)
            make.height.equalTo(100)
        }
    }

    func setupNavigation(){
        navigationItem.title = "Local Notification"
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelButton))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(saveButton))

        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor:UIColor.white]
        navigationItem.rightBarButtonItem?.tintColor = .orange
        navigationItem.leftBarButtonItem?.tintColor = .orange
    }
    
    @objc func cancelButton(){
        self.dismiss(animated: true)
    }
    
    @objc func saveButton(){
        notif.date = datePicker.date
        UserNotification.shared.addNotificationRequest(notif: notif)
        saveNotifDataDelegate?.saveNotifInfo(notifData: notif, index: tempIndexRow)
        self.dismiss(animated: true)
    }
    
}

extension AddNotifVC:UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let item = contentItems[indexPath.row]
        let title = item.title
        switch item {
        case .label(let string), .days(let string), .sounds(let string):
            guard let cell = tableView.dequeueReusableCell(withIdentifier: AddNotifTableViewCell.identifier, for: indexPath) as? AddNotifTableViewCell else{ return UITableViewCell() }
            cell.titleLabel.text = title
            cell.contentLabel.text = string
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let item = contentItems[indexPath.row]
        switch item {
        case .days:
            let repeatVC = RepeatNotifVC()
            repeatVC.repeatDelegate = self
            repeatVC.selectDays = notif.selectDays
            self.navigationController?.pushViewController(repeatVC, animated: true)
            
        case .label:
            let labelVC = NotifLabelVC()
            labelVC.textField.text = notif.note
            labelVC.labelDelegate = self
            self.navigationController?.pushViewController(labelVC, animated: true)

        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}

extension AddNotifVC:UpdateNotifLabelDelegate{
    func updatenotifLabel(notifLabelText: String) {
        notif.note = notifLabelText
    }
}

extension AddNotifVC:UpdateRepeatLabelDelegate{
    
    func updateRepeatLabel(selectedDay: Set<Day>) {
        notif.selectDays = selectedDay
    }
}

protocol UpdateNotifLabelDelegate:AnyObject{
    func updatenotifLabel(notifLabelText: String)
}

protocol UpdateRepeatLabelDelegate:AnyObject{
    func updateRepeatLabel(selectedDay:Set<Day>)
}

extension AddNotifVC {
    enum ContentItem {
        case days(String), label(String), sounds(String)
        
        var title: String {
            switch self {
            case .label: return "Label"
            case .sounds: return "Sounds"
            case .days: return "Hari"
            }
        }
    }
}
