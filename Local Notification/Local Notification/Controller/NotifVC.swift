//
//  NotifVC.swift
//  Local Notification
//
//  Created by Muchamad Fauzi on 02/06/23.
//

import UIKit
import SnapKit

class NotifVC: UIViewController {
    
    var notifStore = NotifStore(){
        didSet{
            notifTableView.reloadData()
        }
    }
    
    let notifTableView: UITableView = {
        let myTable = UITableView(frame: .zero, style: .grouped)
        myTable.separatorStyle = .singleLine
        myTable.register(NotifTableViewCell.self, forCellReuseIdentifier: "notif")
        return myTable
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        notifTableView.dataSource = self
        notifTableView.delegate = self
        setupNavigation()
        setViews()
        setLayouts()
    }
    
    func setViews(){
        self.view.addSubview(notifTableView)
        notifTableView.backgroundColor = .clear
    }
    
    func setLayouts(){
        notifTableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }

    func setupNavigation(){
        navigationItem.title = "Notif"
        navigationItem.leftBarButtonItem = editButtonItem
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addAlarm))
        
        navigationController?.navigationBar.titleTextAttributes = [.foregroundColor:UIColor.white]
        navigationItem.rightBarButtonItem?.tintColor = .orange
        editButtonItem.tintColor = .orange
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        notifTableView.setEditing(editing, animated: true)
    }
    
    @objc func addAlarm(){
        notifStore.isEdit = false
        let vc = AddNotifVC()
        vc.saveNotifDataDelegate = self
        let addAlarmNC = UINavigationController(rootViewController: vc)
        present(addAlarmNC, animated: true, completion: nil)
    }
}

extension NotifVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return notifStore.notifi.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "notif", for: indexPath) as? NotifTableViewCell else {return UITableViewCell()}
            let alarm = notifStore.notifi[indexPath.row]
            
            cell.textLabel?.text = alarm.date.toString(format: "HH:mm")
            cell.detailTextLabel?.text = alarm.noteLabel
            return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        100
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
            return "Reminder Notif"
    }
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.textLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        header.textLabel?.textColor = .white
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
 
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0 { return true }
        return false
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        
        if editingStyle == .delete {
            notifStore.remove(indexPath.row)
        }
    }
}

extension NotifVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            notifStore.isEdit = true
            let vc = AddNotifVC()
            vc.saveNotifDataDelegate = self
            let notif = notifStore.notifi[indexPath.row]
            vc.notif = notif
            vc.tempIndexRow = indexPath.row
            let addNotif = UINavigationController(rootViewController: vc)
            present(addNotif, animated: true, completion: nil)

            tableView.deselectRow(at: indexPath, animated: false)
    }
}

extension NotifVC: SaveNotifInfoDelegate {
    func saveNotifInfo(notifData: NotifInfo, index: Int) {
        if notifStore.isEdit == false{
            notifStore.append(notifData)
        }else{
            notifStore.edit(notifData, index)
        }
    }
}
