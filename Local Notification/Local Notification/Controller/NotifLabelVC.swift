//
//  NotifLabelVC.swift
//  Local Notification
//
//  Created by Muchamad Fauzi on 25/05/23.
//

import UIKit

class NotifLabelVC: UIViewController, UITextFieldDelegate {

    let textField:UITextField = {
       let myTextField = UITextField()
        myTextField.returnKeyType = .done
        myTextField.clearButtonMode = .whileEditing
        myTextField.borderStyle = .roundedRect
        return myTextField
    }()
    
    weak var labelDelegate:UpdateNotifLabelDelegate?
    
    override func viewWillDisappear(_ animated: Bool) {
            
            if let text = textField.text {
                if text == "" {
                    labelDelegate?.updatenotifLabel(notifLabelText: "Notif")
                }else {
                    labelDelegate?.updatenotifLabel(notifLabelText: text)
                }
            }
        }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .brown
        overrideUserInterfaceStyle = .dark
        textField.delegate = self
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        textField.becomeFirstResponder()
    }

    func setupUI(){
        navigationController?.navigationBar.tintColor = .orange
        
        self.title = "Label"
        view.addSubview(textField)
        textField.snp.makeConstraints { make in
            make.leading.equalTo(view.safeAreaLayoutGuide).offset(20)
            make.centerX.equalTo(view.safeAreaLayoutGuide)
            make.centerY.equalTo(view).offset(-120)
            make.height.equalTo(45)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        navigationController?.popViewController(animated: true)
        return true
    }
}
