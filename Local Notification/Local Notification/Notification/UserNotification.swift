//
//  UserNotification.swift
//  Local Notification
//
//  Created by Muchamad Fauzi on 25/05/23.
//

import UIKit
import NotificationCenter

class UserNotification {
    static let shared = UserNotification()
    
    let current = UNUserNotificationCenter.current()

    func addNotificationRequest(notif: NotifInfo) {
        let content = UNMutableNotificationContent()
        content.title = "Reminder"
        content.subtitle = notif.note
        content.categoryIdentifier = "notif"
        content.sound = UNNotificationSound(named: UNNotificationSoundName("call-ringtone.wav"))

        
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: notif.date)
        let minute = calendar.component(.minute, from: notif.date)
        
        if notif.selectDays.isEmpty {
            var dateComponents = DateComponents()
            dateComponents.hour = hour
            dateComponents.minute = minute
            triggerRequest(dateComponents: dateComponents, notif: notif, content: content, isRepeat: false)
        } else {
            let weekdays = notif.selectDays.map { $0.componentWeekday }
            weekdays.forEach { weekDay in
                var dateComponents = DateComponents()
                dateComponents.hour = hour
                dateComponents.minute = minute
                dateComponents.weekday = weekDay
                triggerRequest(dateComponents: dateComponents, notif: notif, content: content)
            }
        }
    }
    
    func triggerRequest(dateComponents: DateComponents, notif: NotifInfo, content: UNMutableNotificationContent, isRepeat: Bool = true) {
        let identifier = "\(notif.id.uuidString)-\(dateComponents.weekday ?? 0)"
        let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: isRepeat)
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        current.add(request) { error in
            if(error == nil){
                print("successfully")
            } else {
                print("error")
            }
        }
    }
}



