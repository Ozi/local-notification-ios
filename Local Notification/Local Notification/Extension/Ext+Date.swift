//
//  Ext+Date.swift
//  Local Notification
//
//  Created by Muchamad Fauzi on 25/05/23.
//

import Foundation

extension Date{
    
    func toString(format: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
    
    var localizedDescription: String {
        return description(with: .current)
    }

}
